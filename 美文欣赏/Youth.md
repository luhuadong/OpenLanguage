# Youth

## 青春

```
Youth is not a time of life; it is a state of mind; it is not a matter of rosy cheeks, red lips and supple knees; it is a matter of the will, a quality of the imagination, a vigor of the emotions; it is freshness of the deep springs of life.
```

青春不是年华，而是心境；青春不是桃面、丹唇、柔膝，而是深沉的意志，恢弘的想象，炙热的恋情；青春是生命的深泉在涌流。



