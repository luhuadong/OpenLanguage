# Summary

* [Introduction](README.md)
* [开言英语](开言英语/README.md)
    * [A1对话](开言英语/A1对话.md)
    * [A2对话](开言英语/A2对话.md)
    * [B1对话](开言英语/B1对话.md)
    * [购物时尚](开言英语/购物时尚.md)
    * [餐厅点餐](开言英语/餐厅点餐.md)
    * [日常沟通](开言英语/日常沟通.md)
    * [家居生活](开言英语/家居生活.md)
    * [休闲娱乐](开言英语/休闲娱乐.md)
    * [社交活动](开言英语/社交活动.md)
    * [个人理财](开言英语/个人理财.md)
    * [租房攻略](开言英语/租房攻略.md)
    * [交通出行](开言英语/交通出行.md)
    * [旅行攻略](开言英语/旅行攻略.md)
    * [饮食文化](开言英语/饮食文化.md)
    * [情感生活](开言英语/情感生活.md)
    * [节目习俗](开言英语/节目习俗.md)
    * [医疗健康](开言英语/医疗健康.md)
    * [应急处理](开言英语/应急处理.md)
    * [运动健身](开言英语/运动健身.md)
    * [科技生活](开言英语/科技生活.md)
    * [职场日常](开言英语/职场日常.md)
    * [电话沟通](开言英语/电话沟通.md)
    * [职业技能](开言英语/职业技能.md)
    * [业务沟通](开言英语/业务沟通.md)
    * [教育留学](开言英语/教育留学.md)
* [高考词汇](高考词汇/README.md)
    * [Module 1](高考词汇/Module_01.md)
    * [Module 2](高考词汇/Module_02.md)
    * [Module 3](高考词汇/Module_03.md)
    * [Module 4](高考词汇/Module_04.md)
    * [Module 5](高考词汇/Module_05.md)
    * [Module 6](高考词汇/Module_06.md)
    * [Module 7](高考词汇/Module_07.md)
    * [Module 8](高考词汇/Module_08.md)
    * [Module 9](高考词汇/Module_09.md)
    * [Module 10](高考词汇/Module_10.md)
* [美文欣赏](美文欣赏/README.md)
    * [Youth](美文欣赏/Youth.md)
* [语法基础](语法基础/README.md)
    * [情态动词](语法基础/情态动词.md)

